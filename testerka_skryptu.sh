#!/usr/bin/env bash

#By Jakub Bratmiński, with Artur Puzio changes

# Usage:
# ./testerka_testerki.sh phone_forward.sh tests_directory parser_executable

DIFF_OPTS="-qN"

if [ "$#" -ne "3" ]; then
    echo "Usage: $0 phone_forward.sh tests_directory parser_executable"
    exit 3 
fi

if [ -z "$1" ]; then
    echo "No program name provided!"
    exit 3
fi
PROG=$1
if [ ! -x "$PROG" ]; then
    echo "$PROG does not exist or is not executable!"
    exit 4
fi

if [ -z "$2" ]; then
    echo "No directory name provided!"
    exit 5
fi
DIR=$2
if [ ! -d "$DIR" ]; then
    echo "$DIR does not exist or is not directory!"
    exit 6
fi

if [ -z "$3" ]; then
    echo "No parser name provided!"
    exit 7
fi
PARSER=$3
if [ ! -x "$PARSER" ]; then
    echo "$PARSER does not exist or is not directory!"
    exit 8
fi

echo "Testing script $PROG on tests from directory $DIR with parser $PARSER:"

pass=0
tests=0

for testfile in $DIR/*.in
do
    testfile=$(basename $testfile .in)
    ciap=`echo $testfile | grep -o '\_[0-9]*$' | tr -d ._`
    
    if [ ! -f $DIR/$testfile.out ]; then
        >&2 echo "$testfile.out does not exist!"
        continue
    fi
    ((++tests))
    
    printf "Testing:    ./$PROG $PARSER $DIR/$testfile.in $ciap    "
    if ! ./$PROG $PARSER $DIR/$testfile.in $ciap \
    > $DIR/$testfile.outb \
    2> $DIR/$testfile.errb; then
        >&2 echo "ERROR 1!"
    elif ! diff $DIFF_OPTS $DIR/$testfile.out $DIR/$testfile.outb 1>&2; then
        >&2 echo "FAIL 2!"
    elif [ -s $DIR/$testfile.errb ]; then
        >&2 echo "ERROR 2!"
    else
        echo "OK"
        ((++pass))
        rm $DIR/$testfile.outb
    fi
done

echo "Done! $pass/$tests tests OK"

if [ "$pass" -eq "$tests" ]; then
    exit 0
else
    exit 1
fi
